/**
 * Created by Admin on 10/6/2017.
 */
function homeController($scope) {
    window._homeCtrl = $scope;

    $scope.tableApi = {};
    $scope.buttonName = "Add item";

    $scope.my_table_options = {
        onRegisterApi: function(api) {
            tableApi = api;
            console.log(api);
        }
    }
    // Table column definition objects
    $scope.my_table_columns = [
        { id: 'ID', key: 'id', label: 'id', lockWidth:true},
        { id: 'first_name', key: 'first_name', label: 'First Name',
            filter:'like',
            template: '<strong>{{row[column.key]}}</strong>',
            labelTemplate:'<div class="my-header-template">{{column.key}}</div>', lockWidth:true
        },
        { id: 'last_name', key: 'last_name', label: 'Last Name', lockWidth:true},
        { id: 'age', key: 'age', label: 'Age', lockWidth:true},
    ];

    // Table data
    $scope.my_table_data = [
        {id:0, first_name: 'sdfsdf cvb', last_name: 'To', age: '27'},
        {id:1, first_name: 'werwe iyu', last_name: 'To', age: '27'},
        {id:2, first_name: 'vbn ertu', last_name: 'To', age: '27'},
        {id:3, first_name: 'ljk asds', last_name: 'To', age: '27'},
        {id:4, first_name: 'hjrgt 657', last_name: 'To', age: '27'},
    ];
    $scope.test = function(){
        $scope.my_table_data = [
            {id:3, first_name: 'ljk asds', last_name: 'To', age: '27'},
            {id:4, first_name: 'hjrgt 657', last_name: 'To', age: '27'}
        ]
        $scope.$apply();
    }
    $scope.edit = function(){
        console.log('change');
        $scope.my_table_data[0] = {id:0, first_name: 'To Hien Nhan', last_name: 'To', age: '28'};
        $scope.$apply();
    }
    $scope.changeColumn = function(){
        $scope.my_table_columns = [
            { id: 'ID', key: 'id', label: 'id', lockWidth:true},
            { id: 'first_name', key: 'first_name', label: 'First Name',
                filter:'like',
                template: '<strong>{{row[column.key]}}</strong>',
                labelTemplate:'<div class="my-header-template">{{column.key}}</div>', lockWidth:true
            },
            { id: 'last_name', key: 'last_name', label: 'Last Name', lockWidth:true}
        ];
    }
}
class page1Controller{
    constructor($scope){
        this.$scope = $scope;
        issueData = [];
    }
}
function DemoController($scope){
    $scope.dataGridOptions = {
        dataSource: [
            {
                "first_name": "To",
                "last_name": "Hien Nhan",
                "email": "nhan@intra.co.kr",
                "age": "17",
                "gender": "male"
            },
            {
                "first_name": "To",
                "last_name": "Nhan hau",
                "email": "hau@gmail.com",
                "age": "10",
                "gender": "male"
            }
        ],
        columns: ["first_name", "last_name", "email", "age", "gender"]
    };
}



var app = angular.module("myApp", [
    'ngAnimate',
    'ngRoute',
    'apMesa',
    'dx'
]);

//Router
app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "view/home.html",
            controller : "homeController"
        })
        .when("/page1", {
            templateUrl : "view/page1.html",
            controller: "page1Controller"
        })
        .when("/page2", {
            templateUrl : "view/page2.html"
        });
});

//Controllers
app.controller("homeController", homeController);
app.controller("page1Controller", page1Controller);
app.controller("DemoController", DemoController);

//Components
app.component('myTable', {
    templateUrl: 'angular-modules/my-table/table-tpl.html',
    controller: tableController
});




