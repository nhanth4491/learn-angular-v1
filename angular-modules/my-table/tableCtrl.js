/**
 * Created by Admin on 10/10/2017.
 */
class tableController {
    constructor($scope, $element) {        
        this.element = $element;
        this.scope = $scope;
        this.table_data = [];
        this.count = 3;
        window._tableCtrl = this;
    }
    $onInit(){
        this.table_data = [
            {id:0, name:'NhanTH', age:27},
            {id:1, name:'Nhan', age:27},
            {id:2, name:'To Hien Nhan', age:27}
        ]
    }

    testAdd(){        
        console.log('test add internal');
        this.table_data.push({id:this.count, name:'New User', age:27});
        this.count++;
    }
    testAddExt(){
        console.log('test add external');
        this.testAdd();
        this.scope.$apply();
    }
}